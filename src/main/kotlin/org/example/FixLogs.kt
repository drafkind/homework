package org.example

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import java.io.BufferedReader
import java.io.FileWriter
import java.io.InputStreamReader
import java.io.PrintWriter
import java.nio.file.Files
import java.util.regex.Pattern

// in real life these should be stored somewhere better
val ACCESS_KEY = "AKIARX7DSUMEJWY5JN5K"
val SECRET_KEY = "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";

// where in s3 to get data from
val LOG_BUCKET = "stellar.health.test.dave.rafkind";
val LOG_KEY = "patients.log";

// interface for an object that can "clean" data lines
interface LineCleaner {

    // given an input data line, return a clean version
    fun clean(inputLine: String): String
}

// object that can "clean" lines of log data, by obfuscating date of birth information
class DateOfBirthLineCleaner : LineCleaner {

    // example log line looks like this:
    // ...... DOB='1/2/3' .....
    val targetPattern = Pattern.compile("(.*)DOB='([^/]+)/([^/]+)/([^/]+)'(.*)");

    // given an input data line, return a cleaned version by obfuscating the date of birth
    override fun clean(inputLine: String): String {
        val matcher = targetPattern.matcher(inputLine);

        if (matcher.matches()) {
            return matcher.replaceAll("$1DOB='X/X/$4'$5")
        } else {
            return inputLine;
        }
    }
}

// main function, ignores command line arguments.
fun main(args: Array<String>) {

    // use hardcoded s3 credentials
    val s3Credentials = BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);

    // create an object to interface to s3
    val s3 = AmazonS3ClientBuilder
        .standard()
        .withRegion(Regions.US_EAST_1) // s3 credentials are tied to this region
        .withCredentials(AWSStaticCredentialsProvider(s3Credentials))
        .build();

    // get the log data from s3
    val s3Object = s3.getObject(LOG_BUCKET, LOG_KEY)

    // create a cleaner object that can clean log lines
    val lineCleaner = DateOfBirthLineCleaner();

    // create a temporary file for saving clean data locally before upload
    val tmpFile = Files.createTempFile("homework", "txt").toFile();

    println(tmpFile)

    // open local file for writing
    PrintWriter(FileWriter(tmpFile)).use { out ->

        // open s3 object for reading
        BufferedReader(InputStreamReader(s3Object.objectContent)).use { reader ->

            // read first line of s3
            var line = reader.readLine();

            // while more to do, do loop body. line will be null if no more data.
            while (line != null) {

                // clean the line we read if possible
                val cleanedUpLine = lineCleaner.clean(line);

                // save the clean line to temp file
                out.println(cleanedUpLine)

                // read the next line
                line = reader.readLine();
            }
        }
    }

    // save temp file back to s3, overwriting original data, as specified by problem statement
    s3.putObject(LOG_BUCKET, LOG_KEY, tmpFile);
}


